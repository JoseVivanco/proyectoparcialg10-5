/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.*;
import entidades.*;

/**Declaracion de la clase Menu
 *
 * @author Jose
 * @version 09/07/2019
 */
public class MenuEmpresas extends Menu {

    public MenuEmpresas(ArrayList<String> opciones) {
        super(opciones);
    }

    @Override
    public void cargarMenu() {
        boolean regresar = false;
        while (!(regresar)) {
            mostrarMenu();
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 5, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    System.out.println();
                    System.out.println("*-----------------------------------------------------------------------------*");
                    Data.mostrarEmpresas();
                    Util.continuar();
                    break;
                case "2":
                    System.out.print("Ingrese el nombre de la empresa : ");
                    String nombreEmpresa = Util.ingresoString();
                    ArrayList<Empresa> resultado = Empresa.comprobarEmpresaNombre(nombreEmpresa);
                    if (!(resultado.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Empresa e : resultado) {
                            System.out.println();
                            System.out.println(e);
                            for (Establecimiento est : e.getEstablecimientos()) {
                                System.out.println();
                                System.out.println(est);
                                for (Promocion p : est.getPromociones()) {
                                    System.out.println();
                                    System.out.println(p);
                                }
                            }
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "3":
                    System.out.print("Ingrese categoría : ");
                    String nCategoria = Util.ingresoString();
                    ArrayList<Empresa> empresasCategoria = Empresa.comprobarEmpresaCategoria(nCategoria);
                    if (!(empresasCategoria.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Empresa e : empresasCategoria) {
                            System.out.println();
                            System.out.println(e);
                            for (Establecimiento est : e.getEstablecimientos()) {
                                System.out.println();
                                System.out.println(est);
                                for (Promocion p : est.getPromociones()) {
                                    System.out.println();
                                    System.out.println(p);
                                }
                            }
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "4":
                    System.out.print("Ingrese sector o ciudad : ");
                    String nUbicacion = Util.ingresoString();
                    ArrayList<Establecimiento> empresasUbicacion = Empresa.comprobarEmpresaUbicacion(nUbicacion);
                    if (!(empresasUbicacion.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Establecimiento est : empresasUbicacion) {
                            System.out.println();
                            System.out.println("Empresa : " + est.getEmpresa());
                            System.out.println();
                            System.out.println(est);
                            for (Promocion p : est.getPromociones()) {
                                System.out.println();
                                System.out.println(p);
                            }
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "5":
                    regresar = true;
                    break;
            }
        }
    }
    /** Metodo añadirOpciones() que posee las distintas opciones del menu de Empresas.
     * 
     * @return 
     */
    public static ArrayList<String> añadirOpciones() {
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Todos");
        opciones.add("Por nombre");
        opciones.add("Por categoría");
        opciones.add("Por ciudad y sector");
        opciones.add("Regresar al menú principal");
        return opciones;
    }
}
