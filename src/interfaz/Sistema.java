/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import entidades.*;
import java.util.*;
import usuarios.*;
import java.time.*;

/**Declaracion de la clase Sistema.
 *
 * @author Jose
 * @version 09/07/2019
 */
public class Sistema {
    // se declara atributo publico.
    public static Persona usuarioActivo;
    /** Metodo cargarSistema() que carga los menus iniciales al correr el programa.
     * 
     */
    public static void cargarSistema() {
        MenuAutenticacion menuAutenticacion = new MenuAutenticacion(MenuAutenticacion.añadirOpciones());
        Util.limpiarPantalla();
        menuAutenticacion.cargarMenu();
    }
    /** Metodo inicializarSistema() que registra por defecto 3 usuarios, 3 empresas, 3 establecimientos y 3 promociones.
     * 
     */
    public static void inicializarSistema() {
        Tarjeta tarjeta1 = new Tarjeta("VISA", "Crédito");
        Tarjeta tarjeta2 = new Tarjeta("MASTERCARD", "Débito");
        Tarjeta tarjeta3 = new Tarjeta("TIA", "Afiliacion");
        ArrayList<Tarjeta> tarjetasUser1 = new ArrayList<>();
        tarjetasUser1.add(tarjeta1);
        tarjetasUser1.add(tarjeta2);
        ArrayList<Tarjeta> tarjetasUser2 = new ArrayList<>();
        tarjetasUser2.add(tarjeta2);
        tarjetasUser2.add(tarjeta3);
        Data.tarjetas.add(tarjeta1);
        Data.tarjetas.add(tarjeta2);
        Data.tarjetas.add(tarjeta3);

        Administrador admin = new Administrador("Brank Malatay", "brank@gmail.com", "brank");
        Tarjetahabiente user1 = new Tarjetahabiente("Jose Garcia", "jose@gmail.com", "jose", Util.toDate("28-07-1999"), "Guayaquil", tarjetasUser1);
        Tarjetahabiente user2 = new Tarjetahabiente("Juan Mendoza", "juan@gmail.com", "juan", Util.toDate("01-09-1999"), "Guayaquil", tarjetasUser2);
        Data.usuarios.add(admin);
        Data.usuarios.add(user1);
        Data.usuarios.add(user2);

        Empresa empresa1 = new Empresa("Tia", "Supermercado", "www.tia.com.ec", "tia", "tia", "tia");
        ArrayList<Dia> dias1 = new ArrayList<>();
        dias1.add(Dia.LUNES);
        dias1.add(Dia.MARTES);
        dias1.add(Dia.MIERCOLES);
        Establecimiento local1 = new Establecimiento("Guayaquil", Sector.NORTE, "Via a Daule", "0999999999".split(","), new LocalTime[] {Util.toTime("10:00"), Util.toTime("21:00")}, empresa1);
        Promocion promocion1 = new Promocion("Descuento 20%", dias1, Util.toDate("10-03-2019"), Util.toDate("10-08-2019") , local1, tarjeta1);
        local1.getPromociones().add(promocion1);
        empresa1.getEstablecimientos().add(local1);
        Data.empresas.add(empresa1);

        Empresa empresa2 = new Empresa("Burger King", "Comida Rápida", "www.burgerking.com.ec", "burgerkingec", "burgerkingec", "burgerkingec");
        ArrayList<Dia> dias2 = new ArrayList<>();
        dias2.add(Dia.JUEVES);
        dias2.add(Dia.VIERNES);        
        Establecimiento local2 = new Establecimiento("Guayaquil", Sector.CENTRO, "9 de octubre", "0999999999".split(","), new LocalTime[] {Util.toTime("10:00"), Util.toTime("21:00")}, empresa2);
        Promocion promocion2 = new Promocion("Descuento 30%", dias2, Util.toDate("10-03-2019"), Util.toDate("10-08-2019"), local2, tarjeta2);
        local2.getPromociones().add(promocion2);
        empresa2.getEstablecimientos().add(local2);
        Data.empresas.add(empresa2);

        Empresa empresa3 = new Empresa("Domino's Pizza", "Comida Rápida", "www.dominos.com.ec", "dominospizza", "dominospizza", "dominospizza");
        ArrayList<Dia> dias3 = new ArrayList<>();
        dias3.add(Dia.SABADO);
        dias3.add(Dia.DOMINGO);        
        Establecimiento local3 = new Establecimiento("Guayaquil", Sector.SUR, "Domingo Comin", "0999999999".split(","), new LocalTime[] {Util.toTime("12:00"), Util.toTime("22:00")}, empresa3);
        Promocion promocion3 = new Promocion("Descuento 40%", dias3, Util.toDate("10-03-2019"), Util.toDate("10-08-2019"), local3, tarjeta3);
        local3.getPromociones().add(promocion3);
        empresa3.getEstablecimientos().add(local3);
        Data.empresas.add(empresa3);
    }
}
